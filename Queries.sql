create table TB3(
	Visit_Type nvarchar(50) not null,
	Grooming bit not null,
	Walking bit not null,
	DeWorming bit not null,
	Diagnostics bit not null,
	Medications bit not null

	primary key (Visit_Type)
);

create table TB2(
	Breed nvarchar(50) not null,
	Classification nvarchar(50) not null,
	Average_height int null,
	Usual_Color nvarchar(50) not null,
	Distinctions nvarchar(50) not null,

	primary key (Breed),
);

create table TB1(
	Name nvarchar(50) not null,
	Breed nvarchar(50) null,
	Length_of_Stay Time(7) not null,
	Type_of_Visit nvarchar(50) not null,

	primary key (Name),
	foreign key (Breed) references TB2 (Breed),
	foreign key (Type_of_Visit) references TB3(Visit_Type)
);

GO

INSERT INTO dbo.TB3 
Values ('Pet_sitting', 1, 0 ,0, 0, 0);

INSERT INTO dbo.TB3 
Values ('Exercise', 1, 1 ,0, 0, 0);

INSERT INTO dbo.TB3 
Values ('Diagnosis', 0, 0 ,0, 1, 0);

INSERT INTO dbo.TB3 
Values ('Re_visit', 0, 0 ,1, 0, 1);

INSERT INTO dbo.TB3 
Values ('Complete_care', 1, 1 ,1, 1, 1);


INSERT INTO dbo.TB2
VALUES ('Pomeranian', 'Dog', 8, 'Blond', 'Tiny');

INSERT INTO dbo.TB2
VALUES ('Siberian Husky', 'Dog', 22, 'Black and White', 'Looks similar to a wolf.');

INSERT INTO dbo.TB2
VALUES ('Savannah Cat', 'Cat', null, 'Calico', 'Serval Hybrid');

INSERT INTO dbo.TB2
VALUES ('Bearded Dragon', 'Lizard', null, 'Yellow', 'Scaley neck.');

INSERT INTO dbo.TB2
VALUES ('Python', 'Snake', null, 'Brown', 'Light Brown patches');


INSERT INTO dbo.TB1
VALUES ('Charles', 'Bearded Dragon', '1 week', 'Complete_care');

INSERT INTO dbo.TB1
VALUES ('Remy', 'Siberian Husky', '2 days', 'Pet_sitting');

INSERT INTO dbo.TB1
VALUES ('Lilah', 'Savannah Cat', '1 days', 'Re_visit');

INSERT INTO dbo.TB1
VALUES ('Sirek', 'Python', '1 week', 'Diagnosis');

INSERT INTO dbo.TB1
VALUES ('Ruffle', 'Pomeranian', '1 week', 'Exercise');

INSERT INTO dbo.TB1
VALUES ('Ranger', 'Great Dane', '1 day', 'Exercise');

GO

UPDATE dbo.TB3
SET Medications = 1
WHERE Visit_Type IN (SELECT Visit_Type 
FROM TB3
Where Visit_Type = 'Diagnosis');

GO

CREATE TRIGGER del
ON dbo.TB1
AFTER DELETE
AS RAISERROR ('Notify Management.', 16, 10);
GO


DELETE FROM dbo.TB1
WHERE Name IN (SELECT Name
FROM TB1
WHERE Name = 'Remy');

GO

SELECT dbo.TB1.Name, dbo.TB2.Classification, dbo.TB1.Length_of_Stay
FROM dbo.TB1
INNER JOIN dbo.TB2 ON dbo.TB2.Breed=dbo.TB1.Breed;

GO

SELECT dbo.TB1.Name, dbo.TB2.Classification, dbo.TB3.Visit_Type, dbo.TB1.Length_of_Stay
FROM dbo.TB1
INNER JOIN dbo.TB2 ON dbo.TB2.Breed=dbo.TB1.Breed
INNER JOIN dbo.TB3 ON dbo.TB3.Visit_type=dbo.TB1.Type_of_Visit;

GO

SELECT dbo.TB2.Distinctions
FROM dbo.TB2
LEFT JOIN dbo.TB1 ON dbo.TB2.Breed=dbo.TB1.Breed;

GO

SELECT dbo.TB1.Name, dbo.TB2.Distinctions, dbo.TB1.Length_of_Stay
FROM dbo.TB2
RIGHT JOIN dbo.TB1 ON dbo.TB2.Breed=dbo.TB1.Breed
ORDER BY dbo.TB1.Name;

GO

Select dbo.TB1.Name, dbo.TB2.Classification, dbo.TB2.Average_height
FROM dbo.TB1
FULL OUTER JOIN dbo.TB2 ON dbo.TB1.Breed=dbo.TB2.Breed;

GO

Select dbo.TB1.*, dbo.TB2.*
FROM dbo.TB1
INNER JOIN TB2 ON TB1.Breed = TB2.Breed;

GO

select dbo.TB1.*
from dbo.TB1
LEFT JOIN dbo.TB2 ON dbo.TB1.Breed=dbo.TB2.Breed;

GO

Select dbo.TB1.*, dbo.TB2.*
FROM dbo.TB1
FULL OUTER JOIN dbo.TB2 ON dbo.TB1.Breed=dbo.TB2.Breed